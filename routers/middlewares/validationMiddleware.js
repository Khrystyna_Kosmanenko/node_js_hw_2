const Joi = require('joi');

const validateRegistration = async (req, res, next) => {
    const schema = Joi.object({
        username: Joi.string()
            .alphanum()
            .min(3)
            .max(30)
            .required(),

        email: Joi.string()
            .email(),
    
        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    });

    await schema.validateAsync(req.body);
    next()
}

module.exports = {validateRegistration}