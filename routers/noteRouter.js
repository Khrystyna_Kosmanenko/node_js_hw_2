const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers')

const { getNones, getOneNote, createNote, changeNote, deleteNote, editNote} = require('../controllers/noteController');
const { authMiddleware } = require('./middlewares/authMiddleware');

router.get('/', authMiddleware, asyncWrapper(getNones))

router.get('/:id', authMiddleware, asyncWrapper(getOneNote))

router.post('/', authMiddleware, asyncWrapper(createNote))

router.delete('/:id', authMiddleware, asyncWrapper(deleteNote))

router.put('/:id', authMiddleware, asyncWrapper(changeNote))

router.patch('/:id', authMiddleware, asyncWrapper(editNote))

module.exports = router