const {Note} = require('../models/noteModel')

const getNones = async(req, res) => {

    const {offset, limit} = req.body;
    const notes = await Note.find({userId: req.user._id}, {__v: 0}).skip(offset).skip(limit)

  if (!notes) {
    res.status(400).json({message: 'notes was not found'})
  }

  res.status(200).json({notes: notes});
}

const getOneNote = async(req, res) => {

  const _id = req.params.id;
  const note = await Note.findById(_id, {_v: 0}).exec()

  if (!note) {
    res.status(400).json({message: 'There is no note with such id'})
  }
  res.status(200).json({note: {_id: note.id, userId: note.userId, completed: note.completed, text: note.text, createdDate: note.createdDate}});
}

const createNote = async(req, res) => {
 
  const text = req.body.text;
  const userId = req.user._id;
  const note = new Note({ text: text, userId: userId, completed: false })
  
  if(!userId) {
    res.status(400).json({message: 'you need to log in'})
  }

  await note.save()
  res.status(200).json({message: 'Success'});
}

const changeNote = async(req, res) => {

  const {text} = req.body;
  const {id} = req.params;

  const note = await Note.findByIdAndUpdate(id, {_v: 0}).exec()
  note.text = text;

  await note.save()
  res.status(200).json({message: 'Success'});
}

const deleteNote = async(req, res) => {
  const {id} = req.params;
  await Note.findByIdAndRemove(id, req.body, err => {
  if(!err) {
      res.status(200).json({message: 'Success'});
    }
  })
}

const editNote = async(req, res) => {
  const {id} = req.params;
  const note = await Note.findById(id, {_v: 0}).exec()
  note.completed = note.completed === true ? false : true;

  await note.save()
  res.status(200).json({message: 'Success'});
}

module.exports = {getNones, getOneNote, createNote, changeNote, deleteNote, editNote}

