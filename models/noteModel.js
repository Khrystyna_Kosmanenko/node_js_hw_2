const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
    text: {
        type: String,
        required: true,
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    completed: {
        type: Boolean,
        default: false,
    },

    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports.Note = mongoose.model('Note', noteSchema);